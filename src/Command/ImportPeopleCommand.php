<?php
/**
 * Created by PhpStorm.
 * User: thomascrouvizier
 * Date: 2019-02-15
 * Time: 16:44
 */

namespace App\Command;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class ImportPeopleCommand extends ContainerAwareCommand
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'restau:people:import';
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            // the short description shown while running "php bin/console list"
            ->setDescription('Creates a new user.')
            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This command allows you to create a user...');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $jSon = file_get_contents("http://35.180.128.163/?method=people");
        $employes = json_decode($jSon);

        foreach ($employes as $employe) {

            $this->userRepository->insertUsers($employe->id, $employe->email, $employe->firstname, $employe->lastname, $employe->jobtitle);
        }
    }
}
