<?php

namespace App\Controller;

use App\Entity\Allergen;
use App\Entity\Dish;
use App\Entity\User;
use phpDocumentor\Reflection\Types\Integer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index()
    {
        return $this->render('admin/menuBack.html.twig', [
            'controller_name' => 'AdminController',
            'page_title' => 'Menu back'
        ]);
    }

    /**
     * @Route("/admin/equipe/inserer", name="admin_team_insert", methods="GET")
     */
    public function service(Request $request)
    {
        $user = new User();

        $form = $this->createFormBuilder($user)
            ->setMethod("GET")
            ->add('username', TextType::class)
            ->add('email', TextType::class)
            ->add('lastname', TextType::class)
            ->add('firstname', TextType::class)
            ->add('jobtitle', ChoiceType::class, [
                'choices' => ['Serveur' => 'Serveur','Chef de rang' => 'Chef de rang']
            ])
            ->add('save', SubmitType::class, ['label' => 'Create User'])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $user = $form->getData();

            // ... perform some action, such as saving the task to the database
            // for example, if Task is a Doctrine entity, save it!
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('front_team');
        }


        return $this->render('admin/index.html.twig', [
            'form' => $form->createView(),
            'page_title' => "Ajout d'un participant"
        ]);
    }


    /**
     * @Route("/admin/carte/ajoutAllergen", name="admin_allergen", methods="GET")
     */
    public function allergen(Request $request)
    {
        $allergen = new Allergen();

        $form = $this->createFormBuilder($allergen)
            ->setMethod("GET")
            ->add('name', TextType::class)
            ->add('save', SubmitType::class, ['label' => 'Create Allergen'])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $user = $form->getData();

            // ... perform some action, such as saving the task to the database
            // for example, if Task is a Doctrine entity, save it!
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('front_dishes');
        }


        return $this->render('admin/addAllergen.html.twig', [
            'form' => $form->createView(),
            'page_title' => 'Ajout Allergen'
        ]);
    }


    /**
     * @Route("/admin/carte/ajoutPlat", name="admin_plat", methods="GET")
     */
    public function dish(Request $request)
    {
        $dish = new Dish();

        $form = $this->createFormBuilder($dish)
            ->setMethod("GET")
            ->add('category', ChoiceType::class)
            ->add('user', ChoiceType::class)
            ->add('name', TextType::class)
            ->add('calories', IntegerType::class)
            ->add('price', IntegerType::class)
            ->add('image', TextType::class)
            ->add('description', TextType::class)
            ->add('sticky', IntegerType::class)
            ->add('save', SubmitType::class, ['label' => 'Create plat'])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $user = $form->getData();

            // ... perform some action, such as saving the task to the database
            // for example, if Task is a Doctrine entity, save it!
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('front_dishes');
        }


        return $this->render('admin/addPlat.html.twig', [
            'form' => $form->createView(),
            'page_title' => 'Ajout plat'
        ]);
    }
}
