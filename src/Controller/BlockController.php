<?php

namespace App\Controller;

use App\Repository\DishRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Dish;

class BlockController extends AbstractController
{
    /**
     * @Route("/block", name="​get_trois_dernier_plats", methods="GET")
     */
    public function DayDishes(DishRepository $repository)
    {
        return $this->render('block/index.html.twig', [
            'controller_name' => 'BlockController',
            'dishs' => $repository->getTroisDernierPlat()
        ]);
    }
}
