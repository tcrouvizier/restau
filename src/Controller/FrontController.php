<?php

namespace App\Controller;

use App\Entity\Dish;
use App\Repository\DishRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class FrontController extends AbstractController
{
    /**
     * @Route("/", name="front_home")
     */
    public function index()
    {
        return $this->render('front/index.html.twig', [
            'controller_name' => 'BlockController',
            'page_title' => 'Accueil'
        ]);
    }

    /**
     * @Route("/mentions-legales", name="front_legals", methods="GET")
     */
    public function mentions()
    {
        return $this->render('front/mentionsLegales.html.twig', [
            'controller_name' => 'FrontController',
            'page_title' => 'Mentions legales'
        ]);
    }

    /**
     * @Route("/equipe", name="front_team", methods="GET")
     */
    public function equipe(UserRepository $repository)
    {
        return $this->render('front/equipe.html.twig', [
            'controller_name' => 'FrontController',
            'users' => $repository->findAll(),
            'page_title' => 'Equipe'
        ]);
    }

    /**
     * @Route("/carte", name="front_dishes", methods="GET")
     */
    public function carte(DishRepository $repository)
    {
        return $this->render('front/carte.html.twig', [
            'controller_name' => 'FrontController',
            'dishs' => $repository->getCategory(),
            'page_title' => 'Carte'
        ]);
    }

    /**
     * @Route("/carte/{id}", name="​front_dishes_category", methods="GET")
     */
    public function carteCategory(DishRepository $repository, int $id)
    {
        return $this->render('front/cartecategory.html.twig', [
            'controller_name' => 'FrontController',
            'dishs' => $repository->getPlatParCategory($id),
            'allergens' => $repository->getAllergen($id),
            'page_title' => 'Plats'
        ]);
    }

}
