<?php

namespace App\Form;

use App\Entity\Category;
use App\Entity\Dish;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DishType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('name', TextType::class, ['required' => true])
            ->add('calories', ChoiceType::class, [
                'choices' => $this->_​availableCalories()])
            ->add('price', NumberType::class, ['required' => true])
            ->add('image', TextType::class, ['empty_data' => 'default.png'])
            ->add('description', TextType::class, ['required' => true])
            ->add('sticky')
            ->add('Category', NumberType::class, ['required' => true])
            ->add('User', IntegerType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Dish::class,
        ]);
    }

    public function _​availableCalories()
    {
        $calories = array();

        for ($i = 10; $i <= 300; $i = $i + 10) {
            $calories[$i] = $i;
        }
        return $calories;
    }
}
