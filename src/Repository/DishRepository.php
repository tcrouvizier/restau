<?php

namespace App\Repository;

use App\Entity\Dish;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Dish|null find($id, $lockMode = null, $lockVersion = null)
 * @method Dish|null findOneBy(array $criteria, array $orderBy = null)
 * @method Dish[]    findAll()
 * @method Dish[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DishRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Dish::class);
    }

    public function getCategory(): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = 'SELECT c.name, COUNT(d.id) as nbPlat, c.id  FROM CATEGORY c LEFT JOIN DISH d ON d.category_id = c.id GROUP BY c.name';

        $stmt = $conn->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    public function getPlatParCategory($id): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = 'SELECT c.name as category, d.name, d.price, d.calories, d.id  FROM CATEGORY c LEFT JOIN DISH d ON d.category_id = c.id WHERE c.id=?';

        $stmt = $conn->prepare($sql);
        $stmt->execute([$id]);

        return $stmt->fetchAll();
    }

    public function getAllergen($id): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = 'SELECT a.name, a.id, ad.dish_id FROM allergen a INNER JOIN allergen_dish ad ON a.id=ad.allergen_id';

        $stmt = $conn->prepare($sql);
        $stmt->execute([$id]);

        return $stmt->fetchAll();
    }

    public function getTroisDernierPlat(): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = "SELECT d.name FROM dish d INNER JOIN category c ON c.id=d.category_id WHERE d.sticky= 1 AND c.name='Plat' ORDER BY d.id DESC LIMIT 3";

        $stmt = $conn->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }
}
