<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function getUsers(): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = 'SELECT * FROM USER';

        $stmt = $conn->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    public function insertUsers(string $id, string $email, string $firstname, string $lastname, string $jobtitle)
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = 'INSERT INTO USER (username, email, firstname, lastname, jobtitle, enabled, createat, updateat) VALUES (?, ?, ?, ?, ?, 0, NOW(), null)';

        $stmt = $conn->prepare($sql);
        $stmt->execute(array(
            $id, $email, $firstname, $lastname, $jobtitle
        ));
    }
}
